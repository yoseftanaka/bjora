<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@getQuestions')->name('home');

Route::group(['prefix' => 'users'], function () {
    Route::group(['middleware' => ['auth', 'check.role:Admin']], function () {
        Route::get('/', 'UserController@getUserList');
        Route::post('/', 'UserController@createUser');
        Route::delete('/{user_id}', 'UserController@deleteUser');
        Route::patch('/{user_id}', 'UserController@updateUser');
        Route::get('/add/form', 'UserController@goToAddForm');
        Route::get('/edit/form/{user_id}', 'UserController@goToEditForm');
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/profiles/{user_id?}', 'UserController@getSingleUser');
        Route::get('/questions', 'QuestionController@getQuestionsByUser');
        Route::get('/edit/profiles/{user_id}', 'UserController@goToEditProfile');
        Route::patch('/update/profiles/{user_id}', 'UserController@editProfile');
        Route::get('/inboxes', 'InboxController@getMessageByUser');
    });
});

Route::group(['prefix' => 'topics', 'middleware' => ['auth', 'check.role:Admin']], function () {
    Route::get('/', 'TopicController@getTopics');
    Route::post('/', 'TopicController@createTopic');
    Route::delete('/{topic_id}', 'TopicController@deleteTopic');
    Route::patch('/{topic_id}', 'TopicController@updateTopic');
    Route::get('/add/form', 'TopicController@goToAddForm');
    Route::get('/edit/form/{topic_id}', 'TopicController@goToEditForm');
});

Route::group(['prefix' => 'questions'], function () {
    Route::group(['middleware' => ['auth', 'check.role:Admin']], function () {
        Route::get('/', 'QuestionController@getQuestions');
        Route::get('/close/{question_id}', 'QuestionController@closeQuestion');
    });

    // Route::get('/{question_id}/get','QuestionController@getSingleQuestion');
    // Route::get('/answers/{question_id}','QuestionController@getAnswersByQuestion');
    Route::group(['middleware' => ['auth']], function () {
        Route::post('/', 'QuestionController@createQuestion');
        Route::delete('/{question_id}', 'QuestionController@deleteQuestion');
        Route::patch('/{question_id}', 'QuestionController@updateQuestion');
        Route::get('/add/form', 'QuestionController@goToAddForm');
        Route::get('/edit/form/{question_id}', 'QuestionController@goToEditForm');
    });

    Route::get('/{questionId}/answers', 'QuestionController@getAnswersByQuestion');
});

Route::group(['prefix' => 'answers', 'middleware' => ['auth']], function () {
    Route::post('/{question_id}', 'AnswerController@createAnswer');
    Route::delete('/{answerId}', 'AnswerController@deleteAnswer');
    Route::patch('/{answerId}', 'AnswerController@updateAnswer');
});

Route::group(['prefix' => 'inboxes', 'middleware' => ['auth']], function () {
    Route::post('/{recieverId}', 'InboxController@createMessage');
    Route::delete('/{messageId}', 'InboxController@deleteMessage');
});

Auth::routes();
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password', 'gender', 'address', 'profile_picture', 'birthday', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function questions(){
        return $this->hasMany('App\Question','member_id');
    }

    public function inboxes(){
        return $this->hasMany('App\Inbox','reciever_id');
    }

    public function inboxesSender(){
        return $this->hasMany('App\Inbox','sender_id');
    }

    public function answers(){
        return $this->hasMany('App\Answer','member_id');
    }
}

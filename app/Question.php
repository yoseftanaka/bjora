<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable=['title','topic_id','status','member_id'];

    public function user(){
        return $this->belongsTo('App\User','member_id');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function topic(){
        return $this->belongsTo('App\Topic');
    }
}

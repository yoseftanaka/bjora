<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['member_id','answer','question_id'];

    public function user(){
        return $this->belongsTo('App\User','member_id');
    }

    public function question(){
        return $this->belongsTo('App\Question');
    }
}

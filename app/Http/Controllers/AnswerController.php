<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Answer;
use Validator;
use App\Question;

class AnswerController extends Controller
{
    //function ini digunakan untuk menyimpan data jawaban ke dalam database
    //function ini melihta terlebih dahulu apakah status question masih terbuka, jika iya ,aka dapat diinput
    public function createAnswer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'answer'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $question = Question::find($request->question_id);
        if ($question->status == "closed") {
            return response()->json([
                'message' => 'this message has been closed'
            ]);
        }

        $answer = new Answer;
        $answer->member_id = Auth::id();
        $answer->question_id = $request->question_id;
        $answer->answer = $request->answer;
        $answer->save();
        return redirect()->back();
    }

    //function ini digunakan untuk mengupdate answer sesuai iputan dan mengecek apakah user tersebut adalah user itu sedniri
    public function updateAnswer(Request $request, $answerId)
    {
        $validator = Validator::make($request->all(), [
            'answer'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $answer = Answer::find($answerId);
        if (Auth::id() != $answer->member_id) {
            return response()->json(['message' => "this answer is not yours"]);
        }
        $answer->answer = $request->answer;
        $answer->save();
        dd($answer);
    }

    //function ini digunakan untuk menghapus data dari database
    public function deleteAnswer($answerId)
    {
        $answer = Answer::find($answerId);
        if(Auth::id()!=$answer->member_id){
            return response()->json(['message'=>"this answer is not yours"]);
        }
        $answer->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inbox;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class InboxController extends Controller
{
    //function ini digunakan untuk menambahkan data message ke user yang memiliki ID yang terpilih
    public function createMessage(Request $request, $recieverId){
        $validator = Validator::make($request->all(),[
            'message' => 'required'
        ]);
        if($validator->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $message = new Inbox;
        $message->reciever_id = $recieverId;
        $message->sender_id = Auth::id();
        $message->message = $request->message;
        $message->save();
        return redirect()->back();
    }

    //function ini digunakan untuk menampilkan semua message berdasarkan user yang sedang login
    public function getMessageByUser(){
        $messages = Inbox::where('reciever_id',Auth::id())->with('user','userSender')->paginate(10);
        return view('inbox',compact('messages'));
    }

    //function ini digunakna unutk menghapus message yang dipilih
    public function deleteMessage($messageId){
        $message = Inbox::find($messageId);
        $message->delete();
        return redirect()->back();
    }
}

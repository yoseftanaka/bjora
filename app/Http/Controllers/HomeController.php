<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //function ini digunakan untuk mendapatkan semua question beserta dengan uuser dan topic yang sesuai.
    //Request yang tertera digunakan untuk search dari nama user atau tulisan question yang ada
    public function getQuestions(Request $request)
    {
        Session::put('backUrl', request()->fullUrl());
        $query = $request->get('query');
        $questions = DB::table('questions')->join('users', 'users.id', '=', 'questions.member_id')->join('topics', 'topics.id', '=', 'questions.topic_id')
            ->where('title', 'LIKE', '%' . $query . '%')->orWhere('name', 'LIKE', '%' . $query . '%')
            ->select('questions.id as question_id', 'title', 'status', 'users.name', 'profile_picture', 'topics.topic_name', 'questions.created_at', 'users.id as user_id')
            ->paginate(10);
        return view('home', compact('questions'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use Validator;

class TopicController extends Controller
{
    //function ini untuk mendapatkan semua topic yang ada di database
    public function getTopics(){
        $topics = Topic::paginate(7);
        return view('masterTopic',compact('topics'));
    }

    //function ini digunakan untuk menyimpan data topic di dalam database
    public function createTopic(Request $request){
        $validator = Validator::make($request->all(), [
            'topic_name'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $topic = new Topic;
        $topic->topic_name = $request->topic_name;
        $topic->save();
        return redirect('/topics');
    }

    //function ini digunakan untuk mn=engubah data topic sesuai denga inputan user
    public function updateTopic(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'topic_name'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        
        $topic = Topic::find($id);
        $topic->topic_name = $request->topic_name;
        $topic->save();
        return redirect('/topics');
    }

    //function ini digunakan untuk menghapus data topic yang dipilih melalui Id dari database
    public function deleteTopic($id){
        $topic = Topic::find($id);
        $topic->delete();

        return redirect('/topics');
    }

    //function ini hanya untuk  menapilkan form untuk menambah topic
    public function goToAddForm(){
        return view('addTopic');
    }


    //function ini hanya digunakna untuk  menampilkan form update topic
    public function goToEditForm($topicId){
        $topic = Topic::find($topicId);
        return view('editTopic',compact('topic'));
    }
}

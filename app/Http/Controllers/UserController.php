<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\User;
use Session;

class UserController extends Controller
{

    //untuk mendapatkan semua data user di dalam database
    //session yang digunakan untuk mencatat url apakah berasal dari link "admin" atau dari link "user"
    public function getUserList()
    {
        Session::put('backUrl', request()->fullUrl());
        $users = User::paginate(10);
        return view('masterUser', compact('users'));
    }

    //untuk mengahpus user yang dipilih dari database
    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/users');
    }

    //untuk menyimpan data user yang diinput ke dalam database
    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6|alpha_num|confirmed',
            'gender' => 'required',
            'address' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg',
            'role' => 'required',
            'birthday' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = $request->file('image');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public', $filename);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->profile_picture = $filename;
        $user->role = $request->role;
        $user->birthday = $request->birthday;
        $user->save();

        return redirect("/users");
    }

    //untuk mengubah data user sesuai yang diinput pada halaman profil
    public function editProfile(Request $request, $userId)
    {
        if ($userId != Auth::id()) {
            return response()->json(['message' => "not yours"]);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'email' => 'required|unique:users,email,'.Auth::id(),
            'password' => 'required|min:6|alpha_num|confirmed',
            'gender' => 'required',
            'address' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg',
            'birthday' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = $request->file('image');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public', $filename);

        $user = User::find(Auth::id());
        $oldpath = 'public/' . $user->profile_picture;
        Storage::delete($oldpath);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->profile_picture = $filename;
        $user->birthday = $request->birthday;
        $user->save();

        return redirect(Session::get('backUrl'));
    }

    //untuk mengubah adta user yang diinput pada halaman edit user
    public function updateUser(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'required|min:6|alpha_num|confirmed',
            'gender' => 'required',
            'address' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg',
            'role' => 'required',
            'birthday' => 'required|date'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = $request->file('image');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('public', $filename);

        $user = User::find($id);
        $oldpath = 'public/' . $user->profile_picture;
        Storage::delete($oldpath);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->profile_picture = $filename;
        $user->birthday = $request->birthday;
        $user->role = $request->role;
        $user->save();

        return redirect('/users');
    }

    //untuk mendapatkan data sebuah user yang dipilih melalui ID.
    //jika ID tidak diberi tau maka secara defauul akan memilih ID user yang sedang login
    //session yang digunakan untuk mencatat url apakah berasal dari link "admin" atau dari link "user"
    public function getSingleUser($userId = null)
    {
        Session::put('backUrl', request()->fullUrl());
        if ($userId == null) $userId = Auth::id();
        $user = User::find($userId);
        return view('profile', compact('user'));
    }

    //function ini untuk menampilkan form menambahkan user
    public function goToAddForm()
    {
        return view('addUser');
    }

    //function ini unutk menampilkan form editUser pada admin
    public function goToEditForm($userId)
    {
        $user = User::find($userId);
        return view('editUser', compact('user'));
    }

    //function ini ini untuk menampilkan form edit pada fitur edit profile
    public function goToEditProfile($userId)
    {
        return view('editProfile', compact('userId'));
    }
}

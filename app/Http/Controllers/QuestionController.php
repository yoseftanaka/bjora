<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Topic;
use Session;
use Validator;

class QuestionController extends Controller
{
    //function ini digunakan untuk mengambil semua qustion beserta dengan user dan topic yang berkaitan untuk page master question
    public function getQuestions($query = "")
    {
        Session::put('backUrl', request()->fullUrl());
        $questions = DB::table('questions')->join('users', 'users.id', '=', 'questions.member_id')->join('topics', 'topics.id', '=', 'questions.topic_id')
            ->where('title', 'LIKE', '%' . $query . '%')->orWhere('name', 'LIKE', '%' . $query . '%')
            ->select('questions.id as question_id', 'title', 'status', 'users.name', 'profile_picture', 'topics.topic_name', 'questions.created_at')
            ->paginate(10);
        return view('masterQuestion', compact('questions'));
    }

    //function ini digunakan unutk mengahpus question dari database
    public function deleteQuestion($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect()->back();
    }

    //function ini digunakan untuk mengupdate question yang ada di dalam database sesuai dengan inputan user
    public function updateQuestion(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'topic_id' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        $question = Question::find($id);
        $question->title = $request->title;
        $question->topic_id = $request->topic_id;
        $question->save();
        return redirect(Session::get('backUrl'));
    }

    //function ini digunakan untuk menyimpan question ke dalam database
    public function createQuestion(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'topic_id' => 'required'
        ]);

        if($validator->fails()){
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }

        $question = new Question;
        $question->title = $request->title;
        $question->topic_id = $request->topic_id;
        $question->member_id = Auth::id();
        $question->save();
        return redirect(Session::get('backUrl'));
    }

    //function ini digunakan untuk megunbah status question dari open menjadi close
    public function closeQuestion($id)
    {
        $question = Question::find($id);
        if ($question->status == "closed") {
            return response()->json(['message' => "already closed"]);
        }
        $question->status = "Closed";
        $question->save();
        return redirect('/questions');
    }

    //function ini unutk mendapatkan semua pertanyaan yang telah dilontarkan oleh user yang terpilih melalui ID
    public function getQuestionsByUser()
    {
        Session::put('backUrl', request()->fullUrl());
        $questions = Question::where('member_id', Auth::id())->with('user', 'topic')->paginate(10);
        return view('myQuestion', compact('questions'));
    }

    //function ini untuk mendapatkan semua jawaban dari sebauh pertanyaan yang telah dipilih
    public function getAnswersByQuestion($questionId)
    {
        $question = Question::with('answers', 'user', 'answers.user')->find($questionId);
        return view('answer', compact('question'));
    }

    //function ini digunakna untuk menampilkan form untuk menambah question
    public function goToAddForm()
    {
        $topics = Topic::all();
        return view('addQuestion', compact('topics'));
    }

    //function ini digunakna untuk menampilkan form untuk mengupdate question
    public function goToEditForm($questionId)
    {
        $topics = Topic::all();
        return view('editQuestion', compact('questionId', 'topics'));
    }
}

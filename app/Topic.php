<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable=['topic_name'];

    public $timestamps = false;

    public function questions(){
        return $this->hasMany('App\Question');
    }
}

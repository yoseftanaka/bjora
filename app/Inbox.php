<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $fillable=['reciever_id','sender_id','message'];

    public function user(){
        return $this->belongsTo('App\User','reciever_id');
    }

    public function userSender(){
        return $this->belongsTo('App\User','sender_id');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Inbox;

class InboxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Inbox::class,30)->create();
    }
}

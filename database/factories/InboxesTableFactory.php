<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Inbox;
use Faker\Generator as Faker;

$factory->define(Inbox::class, function (Faker $faker) {
    return [
        'reciever_id'=>$faker->numberBetween(1,12),
        'sender_id'=>$faker->numberBetween(1,12),
        'message'=>$faker->sentence(10)
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(User::class, function (Faker $faker) {
    $gender=array("Male","Female");
    $role=array("Admin","Member");
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => Hash::make('yoseftanaka'),
        'gender' => $faker->randomElement($gender),
        'address' => $faker->sentence(5),
        'profile_picture' => $faker->image('public/storage',400,300,null,false),
        'birthday' => now(),
        'role' => $faker->randomElement($role)
    ];
});

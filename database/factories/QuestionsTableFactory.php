<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    $status=array("Open","Closed");
    return [
        'title' => $faker->sentence(10),
        'topic_id'=>$faker->numberBetween(1,20),
        'status'=>$faker->randomElement($status),
        'member_id'=>$faker->numberBetween(1,12)
    ];
});

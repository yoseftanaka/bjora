<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    return [
        'member_id' => $faker->numberBetween(1,12),
        'question_id' =>$faker->numberBetween(1,20),
        'answer' => $faker->sentence(10)
    ];
});

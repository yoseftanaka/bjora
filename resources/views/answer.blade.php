@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="answer-wrapper">
                <div class="list-question">
                    <div class="card">
                        <div class="card-header">
                            {{$question->topic->topic_name}}
                        </div>
                        <div class="card-body">
                            <div class="card-title-wrapper d-flex justify-content-between">
                                <h5 class="card-title">{{$question->title}}</h5>
                                <div class="card-status">
                                    @if($question->status == "Open")
                                    <span class="badge badge-success">{{$question->status}}</span>
                                    @else
                                    <span class="badge badge-warning">{{$question->status}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="question-detail-wrapper">
                                <img src="{{ asset('storage/'.$question->user->profile_picture)}}" alt="">
                                <div class="question-detail">
                                    <p class="card-text text-red"><a href="{{url('/users/profiles/'.$question->user->id)}}">{{$question->user->name}}</a></p>
                                    <p class="card-text"><b>Created at</b> : {{$question->created_at}}</p>
                                </div>
                            </div>
                        </div>
                        @foreach($question->answers as $answer)
                        <div class="list-answer col-11 m-auto">
                            <div class="card">
                                <div class="card-body">
                                    <div class="answer-detail-wrapper">
                                        <div class="profile-wrapper">
                                            <img src="{{ asset('storage/'.$answer->user->profile_picture) }}" alt="">
                                            <div class="answer-detail">
                                                <p class="card-text">{{$answer->user->name}}</p>
                                                <p class="card-text"><b>Answered at</b> : {{$answer->created_at}}</p>
                                            </div>
                                        </div>
                                        @if (Auth::user() && Auth::user()->id == $answer->member_id )
                                        <form method="post" action="{{url('/answers/'.$answer->id)}}">
                                            @csrf
                                            @method('delete')
                                            <button class="btn back-red text-white">Delete</button>
                                        </form>
                                        @endif
                                    </div>
                                    <p class="card-text mb-3">{{$answer->answer}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @if (!Auth::guest())
                        @if($question->status == "Open")
                        <div class="card-footer">
                            <form method="POST" action="{{url('/answers/'.$question->id)}}">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-12">
                                        <textarea type="text" class="form-control" placeholder="Add answer..." required name="answer"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <button type="submit" class="btn back-red text-white w-100">
                                            Answer
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
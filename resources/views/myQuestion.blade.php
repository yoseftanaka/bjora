@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="my-question-wrapper">
                <div class="list-question">
                    @foreach($questions as $question)
                    <div class="card mb-2">
                        <div class="card-header">
                            {{$question->topic->topic_name}}
                        </div>
                        <div class="card-body">
                            <div class="card-title-wrapper d-flex justify-content-between">
                                <h5 class="card-title">{{$question->title}}</h5>
                                <div class="card-status">
                                    <!-- <span class="badge badge-warning">Closed</span> -->
                                    @if($question->status == "Open")
                                    <span class="badge badge-success">{{$question->status}}</span>
                                    @else
                                    <span class="badge badge-warning">{{$question->status}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="question-detail-wrapper">
                                <img src="{{ asset('storage/'.$question->user->profile_picture) }}" alt="">
                                <div class="question-detail">
                                    <p class="card-text text-red">{{$question->user->name}}</p>
                                    <p class="card-text"><b>Created at</b> : {{$question->created_at}}</p>
                                </div>
                            </div>
                            <div class="question-settings-button d-flex">
                                <a href="{{ url('questions/'.$question->id.'/answers') }}">
                                    <button class="btn btn-primary mr-2" type="button">See Answer</button>
                                </a>

                                <a href="{{ url('/questions/edit/form/'.$question->id) }}">
                                    <button class="btn btn-warning mr-2">Edit</button>
                                </a>

                                <form method="post" action="{{url('/questions/'.$question->id)}}">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-11">
            <div class="manage-question-wrapper">
                <a href="{{ url('/questions/add/form') }}">
                    <button class="btn back-red text-white">Add Question</button>
                </a>
                <h1><b>Manage Question</b></h1>
                <table class="table question-table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Topic</th>
                            <th scope="col">Owner</th>
                            <th scope="col">Question</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questions as $question)
                        <tr>
                            <th scope="row">{{$question->question_id}}</th>
                            <td>{{$question->topic_name}}</td>
                            <td>{{$question->name}}</td>
                            <td>{{$question->title}}</td>
                            <td>
                                @if($question->status == "Closed")
                                <span class="badge badge-warning">{{$question->status}}</span>
                                @else
                                <span class="badge badge-success">{{$question->status}}</span>
                                @endif
                            </td>
                            <td class="d-flex">
                                @if($question->status == "Open")
                                <a href="{{url('questions/close/'.$question->question_id)}}">
                                    <button class="btn btn-disabled mr-2">Closed</button>
                                </a>
                                @endif
                                <a href="{{ url('/questions/edit/form/'.$question->question_id) }}">
                                    <button class="btn btn-warning mr-2">Edit</button>
                                </a>
                                <form method="post" action="{{url('questions/'.$question->question_id)}}">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$questions->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
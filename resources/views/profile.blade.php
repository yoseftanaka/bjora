@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="profile-detail-wrapper w-100 d-flex justify-content-between align-items-start m-0">
                        <div class="profile-wrapper">
                            <img src="{{ asset('storage/'.$user->profile_picture) }}" alt="" class="align-self-start" />
                            <div class="profile-detail">
                                <p class="card-text card-text-name">{{$user->name}}</p>
                                <p class="card-text">{{$user->email}}</p>
                                <p class="card-text">{{$user->address}}</p>
                                <p class="card-text">{{$user->birthday}}</p>
                            </div>
                        </div>
                        @if(Auth::user() && Auth::user()->id == $user->id)
                        <a href="{{ url('users/edit/profiles/'.$user->id) }}">
                            <button class="btn back-red text-white">Update Profile</button>
                        </a>
                        @endif
                    </div>
                </div>
                @if (Auth::user() && Auth::user()->id != $user->id)
                <div class="card-footer">
                    <form method="POST" action="{{url('/inboxes/'.$user->id)}}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-12">
                                <textarea type="text" class="form-control @error('message') is-invalid @enderror" placeholder="Add message..." name="message"></textarea>
                                @error('message')
                                <span class="invalid-feedback d-flex justify-content-center" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <a href="#">
                                    <button type="submit" class="btn back-red text-white w-100">
                                        Send message
                                    </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
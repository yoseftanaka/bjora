@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card col-md-6 back-light-gray">
            <div class="card-body text-center">
                <form method="POST" action="{{url('questions/'.$questionId)}}">
                    @csrf
                    @method('patch')
                    <h3 class="card-title">Edit Question</h3>
                    <div class="form-group row">
                        <div class="col-12">
                            <textarea id="add-question-input" type="text" class="form-control @error('title') is-invalid @enderror" name="title"></textarea>
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <select class="custom-select @error('topic_id') is-invalid @enderror" id="add-question-input-type" name="topic_id">
                                <option selected value="">Choose...</option>
                                @foreach($topics as $topic)
                                <option value="{{$topic->id}}">{{$topic->topic_name}}</option>
                                @endforeach
                            </select>
                            @error('topic_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" class="btn back-red text-white w-100">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="home-wrapper">
                <form method="GET" action="{{ url('/') }}">
                    <div class="input-group p-0 mb-3 col-6">
                        <input type="text" class="form-control" name="query" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Search by Question or Username">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
                <div class="list-question">
                    @foreach($questions as $question)
                    <div class="card">
                        <div class="card-header">
                            {{$question->topic_name}}
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$question->title}}</h5>
                            <div class="question-detail-wrapper">
                                <img src="{{ asset('storage/'.$question->profile_picture) }}" alt="">
                                <div class="question-detail">
                                    <p class="card-text text-red"><a href="{{url('/users/profiles/'.$question->user_id)}}">{{$question->name}}</a></p>
                                    <p cpnlass="card-text"><b>Created at</b> : {{$question->created_at}}</p>
                                </div>
                            </div>
                            <a href="{{ url('/questions/'.$question->question_id.'/answers') }}" class="btn back-red text-white">Answer</a>
                        </div>
                    </div>
                    @endforeach
                    {{ $questions->appends(request()->except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
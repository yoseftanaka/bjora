@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card col-md-6 back-light-gray">
            <div class="card-body text-center">
                <form method="POST" action="{{url('/topics/')}}">
                    @csrf
                    <h3 class="card-title">Add Topic</h3>
                    <div class="form-group row">
                        <div class="col-12">
                            <input type="text" class="form-control @error('topic_name') is-invalid @enderror" placeholder="Input topic..." name="topic_name">
                            @error('topic_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" class="btn back-red text-white w-100">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md shadow-sm d-flex flex-column back-gray fixed-top">
            <div class="container">
                <a class="navbar-brand text-red" href="{{ url('/') }}">
                    Bjora
                </a>
                @if (!Auth::guest())
                <a class="nav-link text-white" href="{{ url('/users/questions') }}">
                    My Question
                </a>
                <a class="nav-link text-white" href="{{ url('/users/inboxes') }}">
                    Inbox
                </a>
                @if (Auth::user()->role == "Admin")
                <a class="nav-link text-white" href="{{ url('/users') }}">
                    Manage User
                </a>
                <a class="nav-link text-white" href="{{ url('/questions') }}">
                    Manage Question
                </a>
                <a class="nav-link text-white" href="{{ url('/topics') }}">
                    Manage Topic
                </a>
                @endif

                @endif

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @endguest
                        @if (!Auth::guest())
                        <li class="nav-item">
                            <a class="nav-link add-question-nav" href="{{ url('/questions/add/form') }}">
                                Add Question
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{ url('/users/profiles') }}">
                                Profile
                            </a>
                        </li>
                        <li class="nav-item">
                            <form method="post" action="/logout">
                                @csrf
                                <button class="nav-link button-trans" type="submit">Logout</button>
                            </form>
                        </li>
                        @endif

                    </ul>
                </div>
            </div>
            <div class="container">
                <div id="time" class="text-white">Loading current time...</div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        <footer class="footer text-center back-gray">
            <div class="container">
                <span class="text-white">&copy; 2019 Copyright <span class="text-red">Bjora.com</span></span>
            </div>
        </footer>
    </div>
    <script type="text/javascript">
        function showTime() {
            var date = new Date(),
                gmt = new Date(
                    date.getFullYear(),
                    date.getMonth(),
                    date.getDate(),
                    date.getHours(),
                    date.getMinutes(),
                    date.getSeconds()
                );

            document.getElementById('time').innerHTML = gmt.toLocaleString();
        }
        setInterval(showTime, 1000);
    </script>
    <script>
        function addEventHandler(elem, eventType, handler) {
            if (elem.addEventListener)
                elem.addEventListener(eventType, handler, false);
            else if (elem.attachEvent)
                elem.attachEvent('on' + eventType, handler);
        }
        addEventHandler(document, 'DOMContentLoaded', function() {
            addEventHandler(document.getElementById('inputGroupFile01'), 'change', function() {
                var file = this.value;
                if (file) {
                    var startIndex = (file.indexOf('\\') >= 0 ? file.lastIndexOf('\\') : file.lastIndexOf('/'));
                    var filename = file.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                }
                document.getElementById('file-name').innerHTML = filename;
            });
        });
    </script>
</body>

</html>
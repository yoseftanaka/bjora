@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($messages as $message)
        <div class="col-md-8 inbox-container">
            <div class="card mb-2">
                <div class="card-body">
                    <div class="inbox-wrapper w-100 d-flex justify-content-between align-items-start m-0">
                        <div class="profile-wrapper">
                            <img src="{{ asset('img/profile.jpg') }}" alt="" class="align-self-start" />
                            <div class="answer-detail">
                                <p class="card-text card-text-name">{{$message->userSender->name}}</p>
                                <p class="card-text"><b>Posted at</b> : {{$message->created_at}}</p>
                                <p class="card-text"><b>Message</b> : {{$message->message}}</p>

                            </div>
                        </div>
                        <form method="post" action="{{url('/inboxes/'.$message->id)}}">
                        @csrf
                        @method('delete')
                            <button class="btn back-red text-white">Remove</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {{$messages->links()}}
    </div>
</div>
@endsection
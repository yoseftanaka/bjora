@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-11">
            <div class="manage-user-wrapper">
                <a href="{{ url('/users/add/form') }}">
                    <button class="btn back-red text-white">Add User</button>
                </a>
                <h1><b>Manage User</b></h1>
                <table class="table user-table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Role</th>
                            <th scope="col">Email</th>
                            <th scope="col">Fullname</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Address</th>
                            <th scope="col">Profile picture</th>
                            <th scope="col">DOB</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->role}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->gender}}</td>
                            <td>{{$user->address}}</td>
                            <td> <img src="{{ asset('storage/'.$user->profile_picture) }}" alt="" class="align-self-start" />
                            </td>
                            <td>{{$user->birthday}}</td>
                            <td class="d-flex">
                                <a href="{{ url('/users/edit/form/'.$user->id) }}">
                                    <button class="btn btn-warning mr-2">Edit</button>
                                </a>
                                <form action="{{url('/users/'.$user->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
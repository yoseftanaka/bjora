@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-11">
            <div class="manage-topic-wrapper">
                <a href="{{ url('/topics/add/form') }}">
                    <button class="btn back-red text-white">Add Topic</button>
                </a>
                <h1><b>Manage Topic</b></h1>
                <table class="table topic-table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($topics as $topic)
                        <tr>
                            <th scope="row">{{$topic->id}}</th>
                            <td>{{$topic->topic_name}}</td>
                            <td class="d-flex">
                                <a href="{{url('topics/edit/form/'.$topic->id)}}"><button class="btn btn-warning mr-2">Edit</button></a>
                                <form method="post" action="{{url('topics/'.$topic->id)}}">
                                @csrf
                                @method('delete')  
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        {{$topics->links()}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection